package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class PrimeNumber {
    public  static void main(String[] args){
        int[] primeNumber = {2,3,5,7,11};
        System.out.println("THe first prime number is: " + primeNumber[0]);

        ArrayList<String> arrList = new ArrayList<>();
        arrList.add("John");
        arrList.add("Jane");
        arrList.add("Chloe");
        arrList.add("Zoey");
        System.out.println("My friends are: " + arrList);

        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("toothpaste", 15);
        hashMap.put("toothbrush", 20);
        hashMap.put("soap", 12);

        System.out.println("Our current inventory consists of: " + hashMap);
    }

}
